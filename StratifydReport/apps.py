from django.apps import AppConfig


class StratifydreportConfig(AppConfig):
    name = 'StratifydReport'
