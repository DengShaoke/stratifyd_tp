import os

from django.http import HttpResponse
from django.shortcuts import render
import subprocess
# Create your views here.
from StratifydReport.summary_result import deal_data
from StratifydReport.summary_result.deal_data import get_data_list
from stratifyd_TP.settings import BASE_DIR


def show_stress_result(request, file_name=None):
    result_list = get_data_list()
    if file_name is None:
        file_name = result_list[0]
    count_list = deal_data.collect_data(file_name)[1]
    all_list = deal_data.collect_data(file_name)[0]
    return render(request, "stress_show.html", {"count_list": count_list, "all_list": all_list})


def show_stress_list(request):
    result_list = get_data_list()
    return render(request, "stress_list.html", {"result_list": result_list})


def show_ui_report(request, html_report):
    return render(request, html_report)


def git_pull(request):
    res = subprocess.Popen('sh /zhuhaihua/django_report/stratifyd_tp/gitpull.sh', shell=True, stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE, close_fds=True)
    result = res.stdout.readlines()
    return HttpResponse(result)


def upload(request):
    # 请求方法为POST时，进行处理
    if request.method == "POST":
        # 获取上传的文件，如果没有文件，则默认为None
        tag = request.POST.get("tag", "")
        print(tag)
        file = request.FILES.get("send_file", None)
        if file is None:
            return HttpResponse("没有需要上传的文件")
        else:
            # 打开特定的文件进行二进制的写操作
            # print(os.path.exists('/temp_file/'))
            if tag == "":
                get_file = os.path.join(BASE_DIR, "StratifydReport/summary_result/stress_data/{}".format(file.name))
            if tag == "benchmark":
                get_file = os.path.join(BASE_DIR, "StratifydReport/summary_result/stress_data/{}".format(file.name))
            if tag == "ui":
                get_file = os.path.join(BASE_DIR, "templates/{}".format(file.name))
            with open(get_file, 'wb+') as f:
                # 分块写入文件
                for chunk in file.chunks():
                    f.write(chunk)
            return HttpResponse("finished")
    else:
        return render(request, "test_file.html")
