import glob
import json
import os
import time
cur_path = os.path.dirname(os.path.realpath(__file__))
parent_path = os.path.dirname(cur_path)
# csv_path = os.path.join(parent_path, "user_data/stratifyd_page.csv")
# file_path = '/Users/kedeng/PycharmProjects/stratifyd_TP/StratifydReport/summary_result/stress_data/'
file_path = os.path.join(parent_path, "summary_result/stress_data/")

def collect_data(file_name):
    f = file_path + file_name + '.txt'
    with open(f) as ff:
        p = json.load(ff)

    # 处理原始数据中的字段，将带空格的空格去掉，时间戳改为时间
    dd = eval(str(p).replace("amount of data", "numbers"))
    dd.sort(key=lambda x: x["time"])

    # 种类统计
    summary_list = set()
    detail_list = []
    count_list = {}
    for aa in p:
        summary_list.add(str(aa["amount of data"]) + "_" + str(aa["subtype"]) + "_" + str(aa["type"]))
        detail_list.append(
            {str(aa["amount of data"]) + "_" + str(aa["subtype"]) + "_" + str(aa["type"]): aa["duringTime"]})
    for a in summary_list:
        count = 0
        cost_time = 0
        for b in detail_list:
            if a in b:
                cost_time = b[a] + cost_time
                count = count + 1
                avg_time = cost_time / count
                test_number = a.split("_")[0]
                count_list[a] = {"count": count, "cost_time": avg_time, "test_numbers": int(test_number)}
    a = count_list
    b = sorted(a.items(), key=lambda x: x[1]["test_numbers"])
    return dd, b


def get_data_list():
    dir_list = os.listdir(file_path)
    if not dir_list:
        return
    else:
        # 注意，这里使用lambda表达式，将文件按照最后修改时间顺序升序排列
        # os.path.getmtime() 函数是获取文件最后修改时间
        # os.path.getctime() 函数是获取文件最后创建时间
        dir_list = sorted(dir_list, key=lambda x: os.path.getmtime(os.path.join(file_path, x)), reverse=True)
        # print(dir_list)
    file_list = []
    for a in dir_list:
        file_list.append(a.replace(".txt", ""))
    return file_list


def search_all_files_return_by_time_reversed(path, reverse=True):
    return sorted(glob.glob(os.path.join(path, '*')),
                  key=lambda x: time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(os.path.getctime(x))),
                  reverse=reverse)




if __name__ == '__main__':
    print(file_path)

