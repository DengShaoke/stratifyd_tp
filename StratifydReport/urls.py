from django.urls import path
from . import views

urlpatterns =[
    path("file_<slug:file_name>/", views.show_stress_result, name='stress_file_name'),
    path("", views.show_stress_result, name='stress'),
    path("list/", views.show_stress_list, name='list'),
    path("ui/<str:html_report>/", views.show_ui_report, name='ui_report'),
    path("git_pull/", views.git_pull, name='git'),
    path("upload/", views.upload, name='upload'),
]